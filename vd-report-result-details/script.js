$('.vd-report-result-details__chart').highcharts({
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: '<span>Город<br>Екатеринбург',
        align: 'center',
        verticalAlign: 'middle',
        y: -10,
        x:-190,
        style:{"font-size":"12px","color":"rgb(119,119,119)","text-align":"center"}
    },
    tooltip: {
        pointFormat: 'value: {point.y} ({point.percentage:.1f} %)'
    },
    legend: {
        align: 'right',
        verticalAlign: 'top',
        layout: 'vertical',
        x: -200,
        y: 0,
        itemStyle:{"color":"rgb(119,119,119)"},
        itemMarginBottom:20,
        symbolPadding:20,
        symbolRadius:100,
        symbolWidth:12,
        useHTML:true
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                distance: -20,
                format:'{point.percentage:.1f} %',
                style: {
                    fontWeight: 'bold',
                    color: 'white',
                    textShadow: '0px 1px 2px black'
                }
            },
            showInLegend: true,
            center: ['40%', '50%']
        }
    },
    series: [{
        type: 'pie',
        innerSize: '65%',
        data: [
            ['<p>name 1</p><p>sdsada</p>',40],
            ['<p>name 1</p><p>sdsada</p>',40],
            ['<p>name 1</p><p>sdsada</p>',40],
            ['<p>name 1</p><p>sdsada</p>',10],
            ['<p>name 1</p><p>sdsada</p>',15],
            ['<p>name 1</p><p>sdsada</p>',75],
            ['<p>name 1</p><p>sdsada</p>',115],
            ['<p>name 1</p><p>sdsada</p>',15],
            ['<p>name 1</p><p>sdsada</p>',35]
        ]
    }]
});