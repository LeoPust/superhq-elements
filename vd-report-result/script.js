$('.vd-report-result__chart:eq(0)').highcharts({
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: 'Афанасенко<br>Партия «Яблоко»',
        align: 'center',
        verticalAlign: 'middle',
        y: -10,
        style:{"font-size":"10px","color":"rgb(119,119,119)"}
    },
    tooltip: {
        pointFormat: '{point.percentage:.1f} %'
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -20,
                format:'{y} %',
                style: {
                    fontWeight: 'bold',
                    color: 'white',
                    textShadow: '0px 1px 2px black'
                }
            },
            startAngle:0,
            endAngle: 360,
            center: ['50%', '50%']
        }
    },
    series: [{
        type: 'pie',
        innerSize: '65%',
        data: [
            ['name 1',40],
            ['name 2',25],
            ['name 3',35]
        ]
    }]
});