$(function () {
    $('.vd-report-result__chart').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        legend: {
            enabled: false
        },

        series: [{
            type: 'pie',
            name: 'Browser share',
            innerSize: '70%',
            data: [
                ['Firefox',   10.38],
                ['IE',       56.33],
                ['Chrome', 24.03],
                ['Safari',    4.77],
                ['Opera',     0.91],
                ],
            color:'rgb(56,70,87)'
        }]
    });
});